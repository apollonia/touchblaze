"""
Functions to interact with launchctl/launchd
"""


########################################################################
# Package/Module imports
########################################################################
# standard library imports
import os
import plistlib
from xml.parsers.expat import ExpatError, ErrorString


# package specific imports
from version import get_package_version_str
from util import exec_cli


########################################################################
# Atributes
########################################################################
__author__ = "Alan Staniforth <devops@erehwon.xyz>"
__copyright__ = "Copyright (c) 2004-2016 Alan Staniforth"
__license__ = "New-style BSD"
__version__ = get_package_version_str()


########################################################################
# Interface
########################################################################
__all__ = [
            # Classes
            'LaunchdTask', 'TaskError',

            # Constants
            'KEEPALIVE_KEY', 'LABEL_KEY', 'ARGUMENTS_KEY', 'RUNATLOAD_KEY',
            'STARTINTERVAL_KEY', 'USERNAME_KEY'
            ]


########################################################################
# Constants
########################################################################
KEEPALIVE_KEY = 'KeepAlive'
LABEL_KEY = 'Label'
ARGUMENTS_KEY = 'ProgramArguments'
RUNATLOAD_KEY = 'RunAtLoad'
STARTINTERVAL_KEY = 'StartInterval'
USERNAME_KEY = 'UserName'

LAUNCHD_DIR = '/Library/LaunchDaemons/'


########################################################################
# Classes
########################################################################


class TaskError(Exception):
    """
    Exception class for use by LaunchdTask class routines

    Instantiate with an error string

    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class LaunchdTask():
    """
    Class to abstract operations on a launchd task.

    Requires the task label to initialise and You really need to supply the
    command list for the command the task will run. If you don't supply one
    a default command will be supplied which prints a string to std_err
    """
    def __init__(self, label, load=False, user=None, command_list=None,
                 interval=None, keep_alive=None, run_at_load=None,
                 task_dir=None):
        """
        Initiaisation params:

            label:        The standard reverse domain name format for a
                          launchd task.

            load:         True|False - Create the task specified by label by
                          reading from it from a file either in the default
                          directory or from the directory specified by
                          task_dir.
                          If you pass values for any of the other parameters
                          they will replace the values read in from the file.
                          Defaults to False

            user:         The user the task will run as, defaults to root

            command_list: A list made up of the elememts of the command that
                          the task will run. For example

                            ['ls', '-la', '/etc', '>', '/tmp/dir_list']

                          would write the directory listing of /etc to the file
                          /tmp/dir_list. Defaults to:

                            ['(', '>&2', 'echo', "error", ')']

            interval:     sets the "StartInterval" value for the task - how
                          often the task is run. Defaults to 3600 seconds
                          (once every hour).

            keep_alive:   True|False - determines whether the task runs
                          continuously or only on demand or at the
                          'interval' frequency. Defaults to False

            run_at_load:  True|FalseThe task is run immediately on loading.
                          Not best practice so defaults to False

            task_dir:     Specify a directory in which to install this
                          task plist. Defaults to None which means the
                          standard directory 'Library/LaunchDaemons' will
                          be used. This is useful for testing or if you are
                          using th user level ~/Library/LaunchDaemon
        """
        if (label is None) or label is "" or type(label) is not str:
            err = TaskError('Cannot create task object without label')
            raise err
        self.plist_dir = LAUNCHD_DIR if task_dir is None else task_dir
        # sanitise plist_dir
        self.plist_dir = self.plist_dir.rstrip(os.sep) + os.sep
        # set up plist_path
        self.plist_path = self.plist_dir + label + '.plist'
        try:
            if load and os.path.exists(self.plist_path):
                self.task_dict = \
                    plistlib.readPlist(self.plist_path)
            else:
                self.task_dict = self._new_task(label)
        except ExpatError, e:
            print "Error reading plist file: " + str(e.value) + ". Error '"\
                  + ErrorString(e.value) + "'."
        else:
            # update task dict with values passed in __init__ params
            self._update_task_dict(user, command_list, interval, keep_alive,
                                   run_at_load)

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def __str__(self):
        return plistlib.writePlistToString(self.task_dict)

    def _is_launchctl_v2(self):
        """Test to see if have newer version of launchctl"""
        command_list = ['launchctl', 'version']
        command_result = exec_cli(command_list)
        return (command_result is not None)

    def is_loaded(self):
        """Check to see if the daemon task is loaded by launchd"""
        label = self.task_dict[LABEL_KEY]
        command_list = ['launchctl', 'list', '|', 'grep', label]
        command_result = exec_cli(command_list)[0]
        return (len(command_result) is not 0)

    def _update_task_dict(self, user=None, command_list=None, interval=None,
                          keep_alive=None, run_at_load=None, extras=None):
        if user is not None:
            self.task_dict[USERNAME_KEY] = user
        if command_list is not None:
            self.task_dict[ARGUMENTS_KEY] = command_list
        if interval is not None:
            self.task_dict[STARTINTERVAL_KEY] = interval
        if keep_alive is not None:
            self.task_dict[KEEPALIVE_KEY] = keep_alive
        if run_at_load is not None:
            self.task_dict[USERNAME_KEY] = user
        # add any extra fields the user has passed in
        if extras is not None:
            for key in extras:
                self.task_dict[key] = extras[key]

    def _new_task(self, label):
        """
        Create the dictionary representation of a task with default values
        """
        exec_string = \
            "exec(\"import sys\\nimport syslog\\n"\
            + "syslog.syslog(syslog.LOG_ERR, '(" + label\
            + "): Error - no daemon task specified."\
            + "')\\nsys.exit(1)\")"
        DEF_ARGUMENTS = \
            ['/usr/bin/python', '-c', exec_string]
        new_list = {
            KEEPALIVE_KEY: False,
            LABEL_KEY: label,
            ARGUMENTS_KEY: DEF_ARGUMENTS,
            RUNATLOAD_KEY: False,
            STARTINTERVAL_KEY: 3600,
            USERNAME_KEY: 'root'
        }
        return new_list

    def is_installed(self):
        """
        Test if the plist file for the task is installed in
        /Library/LaunchDaemons
        """
        return os.path.exists(self.plist_path)

    def install(self, force=False):
        """Create the plist file for the task"""
        if ((os.path.exists(self.plist_path)) and (force is False)):
            err = TaskError('Task already exists.')
            raise err
        else:
            plistlib.writePlist(self.task_dict, self.plist_path)

    def load(self):
        """Load task to launchd and enable"""
        command_list = ['launchctl', 'load', '-wF', self.plist_path]
        command_result = exec_cli(command_list)
        if command_result[2] is not 0:
            err = TaskError('Error when loading daemon task: '
                            + self.task_dict[LABEL_KEY]
                            + "\nReturn code: " + str(command_result[2])
                            + "\nError code: " + str(command_result[1])
                            + "\nCommand output: " + command_result[0])
            raise err

    def unload(self):
        """Load task to launchd and enable"""
        command_list = ['launchctl', 'unload', '-wF', self.plist_path]
        command_result = exec_cli(command_list)
        if command_result[2] is not 0:
            err = TaskError('Error when unloading daemon task: '
                            + self.task_dict[LABEL_KEY]
                            + "\nReturn code: " + str(command_result[2])
                            + "\nError code: " + str(command_result[1])
                            + "\nCommand output: " + command_result[0])
            raise err

    def set_key_value(self, key=None, value=None, batch_dict=None):
        """
        Set the value of a field in the task dictionary.

        Params:

            key:        The key for the field to be set. This module predefines
                        the constants KEEPALIVE_KEY, LABEL_KEY,
                        ARGUMENTS_KEY, RUNATLOAD_KEY, STARTINTERVAL_KEY and
                        USERNAME_KEY for the keys 'KeepAlive', 'Label',
                        'ProgramArguments', 'RunAtLoad', 'StartInterval'
                        and 'UserName' but you can use any label that
                        launchd recognises.

            value:      The value to with the field indicated by 'key' is to
                        be set. Must be the appropriate type for that field
                        in an XML plist file - for example 'str' for
                        <string>, 'int' for <integer>, True|False for
                        <boolean>, 'list' for <array>, 'dictionary' for
                        <dict> &c.

            batch_dict: a dictionary of key/value pairs to save you having to
                        call this method repeatedly for multiple sets.
        """
        if (key is not None) and (value is not None):
            self.task_dict[key] = value
        if batch_dict is not None:
            self._update_task_dict(extras=batch_dict)

    def get_key_value(self, key=None, batch_list=None):
        """
        Get the value of a field in the task dictionary.

        Params:

            key:    The key for the field to be set. This module predefines
                    the constants KEEPALIVE_KEY, LABEL_KEY, ARGUMENTS_KEY,
                    RUNATLOAD_KEY, STARTINTERVAL_KEY and USERNAME_KEY for
                    the keys 'KeepAlive', 'Label', 'ProgramArguments',
                    'RunAtLoad', 'StartInterval' and 'UserName' but you can
                    use any label that launchd recognises.
        """
        if key is not None:
            return self.task_dict[key]
        if batch_list is not None:
            out_list = []
            for i in batch_list:
                out_list.append(self.task_dict[i])
            return tuple(out_list)
