#!/usr/bin/env python
"""
TouchBlaze daemon script
"""


########################################################################
# Package/Module imports
########################################################################
# standard library imports
import sys
import os
import datetime


########################################################################
# Constants
########################################################################
FILE_INFO_STR = \
"""
This file is regularly updated by a daemon process to stop Backblaze \
reporting an unused Mac as missing.

Last daemon run: """


########################################################################
# Implementation
########################################################################
def main():
    """
    It is assumed that the script is called with the username of the user
    under whose uid launchd is running the script passed as the first argument
    
    Will fail if:
        *   no username passed
        *   the user under whose uid the script is being run does not 
            have write permission for the home directory of the user passed
            in the first argument.
        *   the user passed in the first argument does not exist
    """
    user_name = sys.argv[1]
    touchblaze_dir = "/Users/" + user_name + "/.touchblaze"
    touchblaze_file_path = touchblaze_dir + "/readme.txt"

    if not os.path.exists(touchblaze_dir):
        os.makedirs(touchblaze_dir, 0755)

    touchblaze_file = open(touchblaze_file_path, 'w')
    touchblaze_file.write(FILE_INFO_STR)
    touch_time = datetime.datetime
    touch_time_str = str(touch_time.today()) + '\n'
    touchblaze_file.write(touch_time_str)
    touchblaze_file.close()


########################################################################
# Entry point when run as a script not imported as a module
########################################################################
if __name__ == "__main__":
    main()
