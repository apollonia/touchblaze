Backblaze is a useful service but if you use it on a Mac that runs
headless or at a blanked login screen for a long time you will often
start getting email warnings - "Missing COMPUTER" alerts - from
Backblaze. These will be alerting you that there have been no backups
of the Mac for some time. This happens because Backblaze mostly backs
up user data and a Mac running unused sees very few files if any
change in the monitored directory hierarchies.

TouchBlaze installs (and gives you tools to control) a launchd task that
periodically updates a file in your home directory. That will keep the
Backblaze servers happy and if you now get a "Missing COMPUTER" alert
you will know there is  a real problem.

