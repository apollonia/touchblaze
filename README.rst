==========
TouchBlaze
==========

Stop Backblaze "Missing COMPUTER" warnings

Installation and Set-up
=======================

The best way to install touchblaze is via PyPi and pip.

Install with:

	``pip install touchblaze``

and the touchblaze command should be placed somewhere in your path. On some
systems/installations you may need a:

	``sudo pip install touchblaze``.

Usage
=====

Get help with ``touchblaze -h`` or ``touchblaze --help``

Get the current version with ``touchblaze -v`` or ``touchblaze --version``

In use touchblaze must be invoked with sudo (or as a user with super-user privileges).

To use touchblaze you invoke it followed by a command.

    ``sudo touchblaze status``

Currently touchblaze accepts the following commands:

* ``s`` or ``status``
		This tells touchblaze to report whether the launchd plist is
		installed and whether the task is loaded.

Contribution guidelines
=======================

Feel free to send me pull requests. I have one requirement, all submitted
code must pass checking with flake8_

.. _flake8: http://flake8.pycqa.org

Licence
=======

BSD

Footnotes
=========

.. target-notes::
