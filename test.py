#!/usr/bin/env python
import sys
import os
import touchblaze
from touchblaze.version import get_package_version_str
import touchblaze.launchctl
from touchblaze.launchctl import LaunchdTask
from touchblaze.util import get_real_username, extend_tuple


def main():
    test()


def test():
    test = extend_tuple((1, 2), True, False, 3, [4, 5], {'l1': 6, 'l2': 7}, "a string", 1234567)
    sys.exit(0)
    TASK_LABEL = 'xyz.erehwon.touchblaze'
    user_name = get_real_username()
    mod_path = os.path.dirname(os.path.abspath(touchblaze.__file__))
    script_path = mod_path + "/scripts/daemon.py"
    command_list = [script_path, user_name]
    new_task = LaunchdTask(TASK_LABEL, False, user_name, command_list,
                           interval=3, task_dir=os.getcwd())
    if new_task.is_loaded():
        new_task.unload()
    print new_task
    sys.exit(0)
    new_task.install(force=True)
    new_task.load()


def test2():
    TASK_LABEL = 'xyz.erehwon.touchblaze'
    user_name = get_real_username()
    touch_file_path = '/Users/' + user_name + '/.touchblaze-date'
    command_list = ['date', '>', touch_file_path],
    new_task_plist = _new_launchd_plist(TASK_LABEL, get_real_username(),
                                        command_list, interval=30)
    print new_task_plist
def test1():
    print get_package_version_str()

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()
