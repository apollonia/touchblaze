from setuptools import setup
from touchblaze.version import get_package_version_str


def readme():
    with open('DESCRIPTION.rst') as f:
        return f.read()


setup(name='touchblaze',
      version=get_package_version_str(),
      description='Stop Backblaze "Missing COMPUTER" warnings',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: System :: Archiving :: Backup',
      ],
      keywords='system backblaze launchd backups',
      url='https://bitbucket.org/apollonia/touchblaze',
      author='Erehwon Tools',
      author_email='devops@erehwon.xyz',
      license='MIT',
      download_url='https:\
        //bitbucket.org/apollonia/touchblaze/get/master.tar.gz',
      packages=['touchblaze'],
      zip_safe=False)
